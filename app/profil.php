<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $table = "profil";
    protected $fillable = ['alamat','telp','jenis_kelamin','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function peminjaman()
    {
        return $this->hasMany('App\Peminjaman');
    }
}
