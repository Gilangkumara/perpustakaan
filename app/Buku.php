<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";

    protected $fillable = ['thumbnail', 'nama', 'deskripsi', 'tahun', 'penerbit', 'pengarang', 'kategori_id'];
    // 
    public function kategori()
    {
      return $this->belongsTo('App\Kategori','kategori_id');
    }

    public function peminjaman()
    {
        return $this->hasMany('App\Peminjaman');
    }
  
}
