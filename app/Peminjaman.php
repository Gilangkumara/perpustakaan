<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = ['buku_id','profil_id', 'nama', 'tanggal_pinjam', 'tanggal_balik'];

    public function profil()
    {
        return $this->belongsTo('App\profil');
    }

    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }
}
