@extends('layouts.master')

@section('title')
   Update Profile
@endsection

@section('content')

<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label><strong>Nama</strong></label>
        <input type="text" name="name" value="{{$profil->user->name}}" class="form-control" disabled>
    </div>
    <div class="form-group">
        <label><strong>Email</strong></label>
        <input type="email" name="email" value="{{$profil->user->email}}" class="form-control" disabled>
    </div>
    <div class="form-group">
        <label><strong>Alamat</strong></label>
        <textarea name="alamat" class="form-control">{{$profil->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label><strong>Nomor Telepon</strong></label>
        <input type="number" name="telp" value="{{$profil->telp}}" class="form-control" placeholder="Nomor Telepon">
        
        @error('telp')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label><strong>Jenis Kelamin</strong></label>
        <input type="text" name="jenis_kelamin" value="{{$profil->jenis_kelamin}}" class="form-control" placeholder="Jenis Kelamin">
        
        @error('jenis_kelamin')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection