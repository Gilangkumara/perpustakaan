@extends('layouts.master')

@section('title')
   Edit kategori {{$kategori->nama}}
@endsection

@section('content')

<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" value="{{$kategori->nama}}" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Kategori">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection