@extends('layouts.master')

@section('title')
Peminjaman Buku
@endsection

@section('content')

<a href="/peminjaman/create" class="btn btn-success btn-sm my-2">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Buku</th>
        <th scope="col">Tanggal Peminjaman</th>
        <th scope="col">Tanggal Pengembalian</th>
        <th scope="col">Petugas</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($peminjaman as $key=> $item)
          <tr>
              <td>{{$key + 1 }}</td>  
              <td>{{$item->nama}}</td>   
              <td>{{$item->buku->nama}}</td>
              <td>{{$item->tanggal_pinjam}}</td>
              <td>{{$item->tanggal_balik}}</td>
              <td>{{$item->profil->user->nama}}</td>
              <td>
                <form action="/peminjaman/{{$item->id}}" method="post">
                  @csrf
                  @method('delete')
                    <a href="/peminjaman/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/peminjaman/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                  </form>
              </td>
          </tr>
      @empty
          <tr>
              <td>Data tidak ditemukan</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection