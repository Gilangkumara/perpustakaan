@extends('layouts.master')

@section('title')
   Tambah Buku
@endsection

@section('content')

    <form action="/buku" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Thumbnail</label><br> 
            <input type="file" class="form-control" name="thumbnail">
            @error('thumbnail')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Nama </label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Buku">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Deskrispi </label>
            <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Tahun </label>
            <input type="text" class="form-control" name="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Penerbit </label>
            <input type="text" class="form-control" name="penerbit" placeholder="Masukkan Nama Penerbit">
            @error('penerbit')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Pengarang </label>
            <input type="text" class="form-control" name="pengarang" placeholder="Masukkan Nama Pengarang">
            @error('pengarang')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Kategori </label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">----Pilih Kategori---</option>
                @foreach ($kategori as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option> 
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        

        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    
@endsection