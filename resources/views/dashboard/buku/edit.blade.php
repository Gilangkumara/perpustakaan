@extends('layouts.master')

@section('title')
   Edit Buku
@endsection

@section('content')

    <form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Thumbnail</label><br> 
            <input type="file" class="form-control" value="{{$buku->thumbnail}}" name="thumbnail">
            @error('thumbnail')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Nama </label>
            <input type="text" class="form-control" value="{{$buku->nama}}"name="nama" placeholder="Masukkan Nama Buku">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Deskrispi </label>
            <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$buku->deskripsi}}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Tahun </label>
            <input type="text" class="form-control" value="{{$buku->tahun}}"name="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Penerbit </label>
            <input type="text" class="form-control" value="{{$buku->penerbit}}"name="penerbit" placeholder="Masukkan Nama Penerbit">
            @error('penerbit')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Pengarang </label>
            <input type="text" class="form-control" value="{{$buku->pengarang}}" name="pengarang" placeholder="Masukkan Nama Pengarang">
            @error('pengarang')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Kategori </label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">----Pilih Kategori---</option>
                @foreach ($kategori as $item)
                    @if ($item->id === $buku->kategori_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option> 
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                    
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        
        <button type="submit" class="btn btn-primary"> Update </button>
    </form>

@endsection